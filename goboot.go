/*
Copyright 2021 The Beijing Gridsum Technology Co., Ltd Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package goboot

import (
	"fmt"
	paho "github.com/eclipse/paho.mqtt.golang"
	"github.com/spf13/viper"
	"io"
	"os"
)

// Logger represents an abstracted structured logging implementation.
type Logger interface {
	Debug(...interface{})
	Info(...interface{})
	Error(...interface{})
	Fatal(items ...interface{})
	Panic(items ...interface{})
	Warn(items ...interface{})
	WithField(string, interface{}) Logger

	// Writer Logger can be transformed into an io.Writer.
	Writer() *io.PipeWriter

	Println(items ...interface{})
	Printf(format string, items ...interface{})
}

// Client describes an immutable MQTT client.
type Client interface {
	Connect() error
	Disconnect()
	SetClose() // retry and close distinction handle.
	// Publish publishes provided messages to given topic.
	//
	// Publish can be synchronous or asynchronous - it depends on the implementation.
	//
	// Most publishers implementations don't support atomic publishing of messages.
	// This means that if publishing one of the messages fails, the next messages will not be published.
	//
	// Publish must be thread safe.
	Publish(topic string, qos int, retained bool, message []byte) error
	Subscribe(topic string, qos int, onMessage paho.MessageHandler) error
	IsConnected() bool
}

type Service interface {
	Open() error
	Close() error
}

// LookupEnv returns the value in the environment, if any.
func LookupEnv(v *viper.Viper, envVar string) interface{} {
	if envVar == "" {
		return nil
	}
	return v.Get(envVar)
}

// CheckErr prints the msg with the prefix 'Error:' and exits with error code 1. If the msg is nil, it does nothing.
func CheckErr(msg interface{}) {
	if msg != nil {
		fmt.Fprintln(os.Stderr, "Error:", msg)
		os.Exit(1)
	}
}
