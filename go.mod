module gitlab.com/taoshumin/goboot

go 1.16

require (
	github.com/eclipse/paho.mqtt.golang v1.4.1
	github.com/google/uuid v1.3.0
	github.com/lithammer/shortuuid/v3 v3.0.7
	github.com/oklog/ulid v1.3.1
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cast v1.4.1
	github.com/spf13/viper v1.9.0
	golang.org/x/net v0.0.0-20210917221730-978cfadd31cf // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
