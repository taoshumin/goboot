/*
Copyright 2021 The Beijing Gridsum Technology Co., Ltd Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package utils

import "testing"

func TestNewShortUUID(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "short",
			want: "YNR3RG4XE2WwopZLPq6fsd",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewShortUUID()
			if got != tt.want {
				t.Errorf("NewShortUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewULID(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "ULID",
			want: "01FFSF480BBFQNAE2YCM1S7E6F",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewULID()
			if got != tt.want {
				t.Errorf("NewULID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewUUID(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "uuid",
			want: "620b0fc7-62e4-4747-9080-eb0e83d17011",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewUUID()
			if got != tt.want {
				t.Errorf("NewUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}
