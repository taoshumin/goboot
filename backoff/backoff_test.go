/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package backoff

import (
	"errors"
	"testing"
	"time"
)

func TestBackoff(t *testing.T) {
	okAfter := 2
	calls := 0

	err := Do(100*time.Millisecond, 1*time.Second, 3, func() error {
		calls++
		if calls > okAfter {
			return nil
		}
		return errors.New("not ok")
	})
	if err != nil {
		t.Fatalf("Error should be nil but is: %v", err)
	}
	if calls > 3 {
		t.Fatalf("Calls should be < 10 but is: %v", calls)
	}
}

func TestBackoffMaxCalls(t *testing.T) {
	okAfter := 5
	calls := 0

	err := Do(100*time.Millisecond, 1*time.Second, 2, func() error {
		calls++
		if calls > okAfter {
			return nil
		}
		return errors.New("not ok")
	})
	if err == nil {
		t.Fatalf("Error should be not nil but is: %v", err)
	}
	if calls != 2 {
		t.Fatalf("Calls should be 2 but is: %v", calls)
	}
}
