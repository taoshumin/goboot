/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"fmt"
	"gitlab.com/taoshumin/goboot"
	"gitlab.com/taoshumin/goboot/backoff"
	"strings"
	"time"
)

type PublishService struct {
	s   *Service
	c   Rules
	log goboot.Logger
}

func NewPublishService(s *Service, c Rules, log goboot.Logger) *PublishService {
	return &PublishService{s: s, c: c, log: log}
}

func (s *PublishService) Open() error {
	if err := s.s.Open(); err != nil {
		return err
	}
	return nil
}

func (s *PublishService) OnlyPub() (configs []HandlerConfig) {
	for _, rule := range s.c {
		configs = append(configs, *rule.Pub)
	}
	return
}

func (s *PublishService) Publish(message []byte) error {
	var errs []string
	for _, h := range s.OnlyPub() {
		//  validate pub config.
		if err := h.Validate(); err != nil {
			errs = append(errs, err.Error())
			continue
		}
		err := backoff.Do(100*time.Millisecond, 1*time.Second, 3, func() error {
			return s.s.Publish(HandlerConfig{
				Name:     h.Name,
				ClientID: h.ClientID,
				Topic:    h.Topic,
				QoS:      h.QoS,
				Retained: h.Retained,
				Body:     message,
			})
		})
		if err != nil {
			errs = append(errs, err.Error())
		}
	}

	if len(errs) != 0 {
		return fmt.Errorf("publishes server: [%s]", strings.Join(errs, ","))
	}
	return nil
}

func (s *PublishService) Close() error {
	if err := s.s.Close(); err != nil {
		return err
	}
	return nil
}
