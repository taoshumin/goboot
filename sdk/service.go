/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/taoshumin/goboot"
	"gitlab.com/taoshumin/goboot/backoff"
	"sync"
	"time"
)

type Service struct {
	mu      sync.RWMutex
	clients map[string]goboot.Client
	configs map[string]Config
}

func NewService(cs Configs) (*Service, error) {
	configs := cs.index()
	clients := make(map[string]goboot.Client, len(cs))

	for name, c := range configs {
		cli, err := c.NewClient()
		if err != nil {
			return nil, err
		}
		clients[name] = cli
	}

	svc := &Service{
		configs: configs,
		clients: clients,
	}
	return svc, nil
}

func (s *Service) Open() error {
	s.mu.Lock()
	defer s.mu.Unlock()
	for name, client := range s.clients {
		if client == nil {
			return fmt.Errorf("no client found for MQTT broker %q", name)
		}
		// When the container is restarted, it keeps retrying if the connection fails.
		err := backoff.Do(3*time.Second, 30*time.Minute, 0, func() error {
			if err := client.Connect(); err != nil {
				return errors.Wrapf(err, "failed to connect to MQTT broker %q", name)
			}
			return nil
		})

		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Service) Update(newConfigs []interface{}) error {
	cs := make(Configs, len(newConfigs))
	for i, c := range newConfigs {
		config, ok := c.(Config)
		if !ok {
			return fmt.Errorf("expected config object to be of type %T, got %T", config, c)
		}
		cs[i] = config
	}
	return s.update(cs)
}

func (s *Service) update(cs Configs) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if err := cs.Validate(); err != nil {
		return err
	}

	configs := cs.index()
	for name, c := range configs {
		old, ok := s.configs[name]
		if ok && old.Equal(c) {
			continue
		}
		client := s.clients[name]

		if client != nil {
			client.Disconnect()
		}
		s.clients[name] = nil

		client, err := c.NewClient()
		if err != nil {
			return err
		}

		if err := client.Connect(); err != nil {
			return err
		}
		s.clients[name] = client
	}

	// Disconnect and remove old clients
	for name := range s.configs {
		if _, ok := configs[name]; !ok {
			client := s.clients[name]
			if client != nil {
				client.Disconnect()
			}
			delete(s.clients, name)
		}
	}
	s.configs = configs
	return nil
}

func (s *Service) ClientByHashID(hash string) goboot.Client {
	return s.clients[hash]
}

func (s *Service) Publish(h HandlerConfig) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if h.Topic == "" {
		return fmt.Errorf("missing MQTT topic")
	}

	if h.Name == "" {
		return fmt.Errorf("missing MQTT broker name")
	}

	if h.ClientID == "" {
		return fmt.Errorf("missing MQTT client id")
	}

	client := s.clients[HashID(h.Name, h.ClientID)]
	if client == nil {
		return fmt.Errorf("unknown MQTT broker %q ClientId %q", h.Name, h.ClientID)
	}

	return backoff.Do(100*time.Millisecond, 1*time.Second, 3, func() error {
		return client.Publish(h.Topic, h.QoS, h.Retained, h.Body)
	})
}

func (s *Service) Close() error {
	s.mu.Lock()
	defer s.mu.Unlock()
	for _, client := range s.clients {
		if client != nil {
			client.SetClose()
			client.Disconnect()
		}
	}
	return nil
}
