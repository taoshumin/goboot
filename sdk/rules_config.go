/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"errors"
	"fmt"
	"gitlab.com/taoshumin/goboot/utils"
)

const (
	PUB = "CPSPUB"
	SUB = "CPSSUB"
)

type Rules []Rule

type Rule struct {
	Sub *HandlerConfig `mapstructure:"sub"`
	Pub *HandlerConfig `mapstructure:"pub"`
}

func (r Rules) Validate() error {
	for _, c := range r {
		if c.Sub != nil {
			if err := c.Sub.Validate(); err != nil {
				return err
			}
		}

		if c.Pub != nil {
			if err := c.Pub.Validate(); err != nil {
				return err
			}
		}
	}
	if len(r) == 0 {
		return errors.New("missing Rules")
	}
	return nil
}

type RuleConfig struct {
	Mqtts Configs `mapstructure:"mqtts"`
	Rules Rules   `mapstructure:"rules"`
}

func NewRuleConfig(mqtt Configs, rules Rules) RuleConfig {
	return RuleConfig{
		Mqtts: mqtt,
		Rules: rules,
	}
}

func (c RuleConfig) Parse() (*RuleConfig, error) {
	mqtt := make(map[string]Config)
	for _, mq := range c.Mqtts {
		mqtt[mq.Name] = mq
	}

	re := &RuleConfig{}
	for _, r := range c.Rules {

		if r.Pub != nil {
			// Pub rule mqtt
			pub, pe := mqtt[r.Pub.Name]
			if pe {
				re.Mqtts = append(re.Mqtts, pub.SetClientId(r.Pub.ClientID))
			}
		}

		// Sub rule mqtt
		if r.Sub != nil {
			sub, se := mqtt[r.Sub.Name]
			if se {
				re.Mqtts = append(re.Mqtts, sub.SetClientId(r.Sub.ClientID))
			}
		}

		if r.Sub != nil || r.Pub != nil {
			re.Rules = append(re.Rules, r)
		}
	}

	if len(re.Mqtts) == 0 {
		return nil, errors.New("missing MQTTs Config")
	}
	if len(re.Rules) == 0 {
		return nil, errors.New("missing Rules Config")
	}
	return re, nil
}

func (c RuleConfig) ParseSetClientID() (*RuleConfig, error) {
	mqtt := make(map[string]Config)
	for _, mq := range c.Mqtts {
		mqtt[mq.Name] = mq
	}

	re := &RuleConfig{}
	for _, r := range c.Rules {

		if r.Pub != nil {
			// Pub rule mqtt
			pub, pe := mqtt[r.Pub.Name]
			if pe {
				clientId := fmt.Sprintf("%s-%s", PUB, utils.NewShortUUID())
				r.Pub.ClientID = clientId
				re.Mqtts = append(re.Mqtts, pub.SetClientId(clientId))
			}
		}

		// Sub rule mqtt
		if r.Sub != nil {
			sub, se := mqtt[r.Sub.Name]
			if se {
				clientId := fmt.Sprintf("%s-%s", SUB, utils.NewShortUUID())
				r.Sub.ClientID = clientId
				re.Mqtts = append(re.Mqtts, sub.SetClientId(clientId))
			}
		}

		if r.Sub != nil || r.Pub != nil {
			if r.Sub != nil {
				r.Sub.QoS = 1
			}
			if r.Pub != nil {
				r.Pub.QoS = 1
			}
			re.Rules = append(re.Rules, r)
		}
	}

	if len(re.Mqtts) == 0 {
		return nil, errors.New("missing MQTTs Config")
	}
	if len(re.Rules) == 0 {
		return nil, errors.New("missing Rules Config")
	}
	return re, nil
}
