/*
Copyright 2021 The Beijing Gridsum Technology Co., Ltd Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"fmt"
	paho "github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/taoshumin/goboot"
	"gitlab.com/taoshumin/goboot/tlsconfig"
	"gitlab.com/taoshumin/goboot/utils"
	"time"
)

// newClient produces a disconnected MQTT client
var newClient = func(c Config) (goboot.Client, error) {
	opts := paho.NewClientOptions()
	opts.AddBroker(c.Broker)

	if c.ClientID != "" {
		opts.SetClientID(c.ClientID)
	} else {
		opts.SetClientID(utils.NewShortUUID())
	}

	opts.SetUsername(c.Username)
	opts.SetPassword(c.Password)

	if c.SSLCA != "" && c.SSLCert != "" && c.SSLKey != "" {
		tlsConfig, err := tlsconfig.Create(c.SSLCA, c.SSLCert, c.SSLKey, c.InsecureSkipVerify)
		if err != nil {
			return nil, err
		}
		opts.SetTLSConfig(tlsConfig)
	}

	cc := &PahoClient{
		opts:  opts,
		ch:    make(chan bool),
		close: make(chan struct{}),
	}
	opts.SetOnConnectHandler(cc.OnConnect)
	return cc, nil
}

type PahoClient struct {
	client paho.Client
	opts   *paho.ClientOptions
	ch     chan bool
	close  chan struct{}
	first  int
}

func (p *PahoClient) OnConnect(client paho.Client) {
	if p.first != 0 && p.ch != nil {
		p.client = client
		p.ch <- true
	}
	p.first++
}

func (p *PahoClient) Connect() error {
	// Using a clean session forces the broker to dispose of client session
	// information after disconnecting. Retention of this is useful for
	// constrained clients.  Since Kapacitor is only publishing, it has no
	// storage requirements and can reduce load on the broker by using a clean
	// session.
	p.opts.SetCleanSession(false)

	p.client = paho.NewClient(p.opts)
	token := p.client.Connect()
	token.Wait()
	return token.Error()
}

func (p *PahoClient) SetClose() {
	close(p.close)
	p.close = nil
}

func (p *PahoClient) Disconnect() {
	if p.close != nil {
		p.close <- struct{}{}
	}
	if p.client != nil {
		p.client.Disconnect(uint(DefaultQuiesceTimeout / time.Millisecond))
	}
}

func (p *PahoClient) Publish(topic string, qos int, retained bool, message []byte) error {
	token := p.client.Publish(topic, byte(qos), retained, message)
	ok := token.WaitTimeout(3 * time.Second)
	err := token.Error()
	if !ok || err != nil {
		return fmt.Errorf("qos: %d,publish messsge error", qos)
	}
	go func() {
		for {
			select {
			case <-p.ch:
				token := p.client.Publish(topic, byte(qos), retained, message)
				ok := token.WaitTimeout(3 * time.Second)
				err := token.Error()
				if !ok || err != nil {
					fmt.Errorf("qos: %d,publish messsge error", qos)
				}
			case <-p.close:
				return
			}
		}
	}()
	return nil
}

func (p *PahoClient) Subscribe(topic string, qos int, onMessage paho.MessageHandler) error {
	token := p.client.Subscribe(topic, byte(qos), onMessage)
	ok := token.WaitTimeout(3 * time.Second)
	err := token.Error()
	if !ok || err != nil {
		return fmt.Errorf("qos: %d,publish messsge error", qos)
	}

	go func() {
		for {
			select {
			case <-p.ch:
				token := p.client.Subscribe(topic, byte(qos), onMessage)
				ok := token.WaitTimeout(3 * time.Second)
				err := token.Error()
				if !ok || err != nil {
					fmt.Errorf("qos: %d,publish messsge error", qos)
				}
			case <-p.close:
				return
			}
		}
	}()

	return nil
}

func (p *PahoClient) IsConnected() bool {
	return p.client.IsConnected()
}
