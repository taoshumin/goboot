/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"encoding/json"
	"github.com/spf13/cast"
	"github.com/spf13/viper"
	"gitlab.com/taoshumin/goboot"
	"gitlab.com/taoshumin/goboot/logger"
)

type MQTTConfig struct {
	MQTTS  Configs       `mapstructure:"mqtts"`
	Rules  Rules        `mapstructure:"rules"`
	Logger logger.Config `mapstructure:"logger"`
}

func NewMQTTConfig() *MQTTConfig {
	return &MQTTConfig{
		Logger: logger.NewConfig(),
	}
}

func (c *MQTTConfig) BindEnv(v *viper.Viper) error {
	// set logger env
	if varEnv :=goboot.LookupEnv(v,"LOGGER.LEVEL"); varEnv !=nil{
		if s,err := cast.ToStringE(varEnv);err == nil{
			c.Logger.Level  = s
		}
	}

	// set mqtts env
	if varEnv :=goboot.LookupEnv(v,"MQTTS"); varEnv !=nil{
		if s,err := cast.ToStringE(varEnv);err == nil{
			if err:=json.Unmarshal([]byte(s),&c.MQTTS);err!=nil{
				return err
			}
		}
	}

	// set rules env
	if varEnv :=goboot.LookupEnv(v,"RULES"); varEnv !=nil{
		if s,err := cast.ToStringE(varEnv);err == nil{
			if err:= json.Unmarshal([]byte(s),&c.Rules);err!=nil{
				return err
			}
		}
	}
	return nil
}