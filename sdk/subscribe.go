/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"fmt"
	paho "github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/taoshumin/goboot"
	"gitlab.com/taoshumin/goboot/backoff"
	"golang.org/x/sync/errgroup"
	"time"
)

type SubscribeService struct {
	s   *Service
	c   Rules
	p   Processor
	wg  errgroup.Group
	log goboot.Logger
}

func NewSubscribeService(s *Service, p Processor, c Rules, log goboot.Logger) *SubscribeService {
	return &SubscribeService{s: s, p: p, c: c, log: log}
}

func (s *SubscribeService) Open() error {
	if err := s.s.Open(); err != nil {
		return err
	}
	return s.Subscribe(s.c)
}

func (s *SubscribeService) Close() error {
	if err := s.s.Close(); err != nil {
		return err
	}

	if err := s.wg.Wait(); err != nil {
		return err
	}
	return nil
}

func (s *SubscribeService) Subscribe(hs Rules) error {
	for _, v := range hs {
		h := v

		// validate sub config.
		if h.Sub == nil {
			return fmt.Errorf("missing mqtt sub client")
		}

		if err := h.Sub.Validate(); err != nil {
			return err
		}

		if h.Pub != nil {
			//  validate pub config.
			if err := h.Pub.Validate(); err != nil {
				return err
			}
		}

		s.wg.Go(func() error {
			// Hash for goboot client.
			client := s.s.ClientByHashID(HashID(h.Sub.Name, h.Sub.ClientID))
			if client == nil {
				return fmt.Errorf("unknown MQTT broker %q ClientId %q", h.Sub.Name, h.Sub.ClientID)
			}

			// client subscribe mqtt.
			err := backoff.Do(100*time.Millisecond, 1*time.Second, 3, func() error {
				return client.Subscribe(h.Sub.Topic, h.Sub.QoS, func(client paho.Client, message paho.Message) {
					// filter subscription service.
					if h.Pub != nil && s.p.Filter(*h.Sub) {
						if err := s.p.Process(HandlerConfig{
							Name:     h.Pub.Name,
							ClientID: h.Pub.ClientID,
							Topic:    h.Pub.Topic,
							QoS:      h.Pub.QoS,
							Retained: h.Pub.Retained,
							Body:     message.Payload(),
							Client:   s,
						}); err != nil {
							s.log.
								WithField("error_type", "error").
								WithField("error_msg", err).
								Error(fmt.Sprintf("MQTT client process message from topic: %s, name: %s", h.Sub.Topic, h.Sub.Name))
						}
					}

					// Subscription service use without Pub
					// just consume data，no publish
					if h.Pub == nil && s.p.Filter(*h.Sub) {
						if err := s.p.Process(HandlerConfig{
							Body: message.Payload(),
						}); err != nil {
							s.log.
								WithField("error_type", "error").
								WithField("error_msg", err).
								Error(fmt.Sprintf("MQTT client process message from topic: %s, name: %s", h.Sub.Topic, h.Sub.Name))
						}
					}
				})
			})

			if err != nil {
				s.log.
					WithField("error_type", "error").
					WithField("error_msg", err).
					Error(fmt.Sprintf("MQTT client subscribe message from topic: %s, name: %s", h.Sub.Topic, h.Sub.Name))
			}
			return nil
		})
	}
	return nil
}

func (s *SubscribeService) Publish(hs HandlerConfig) error {
	return s.s.Publish(hs)
}
