/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import "fmt"

// HandlerConfig define sub and pub config.
type HandlerConfig struct {
	Name     string            `mapstructure:"name"`
	ClientID string            `mapstructure:"clientid"`
	Topic    string            `mapstructure:"topic"`
	QoS      int               `mapstructure:"qos"`
	Retained bool              `mapstructure:"retained"`
	Body     []byte            `mapstructure:"-"`
	Client   *SubscribeService `mapstructure:"-"`
}

func (h HandlerConfig) Validate() error {
	if h.Topic == "" {
		return fmt.Errorf("missing MQTT topic")
	}
	if h.Name == "" {
		return fmt.Errorf("missing MQTT broker name")
	}
	if h.ClientID == "" {
		return fmt.Errorf("missing MQTT client id")
	}
	return nil
}

// Processor is a processor plugin interface for handler message.
type Processor interface {
	Filter(config HandlerConfig) bool
	Process(config HandlerConfig) error
}

type noopProcess struct{}

func (s *noopProcess) Process(config HandlerConfig) error { return nil }

func (s *noopProcess) Filter(config HandlerConfig) bool { return true }
