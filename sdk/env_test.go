/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"github.com/spf13/viper"
	"os"
	"testing"
)

func TestMQTTConfig_BindEnv(t *testing.T) {

	tests := []struct {
		name    string
		fields   *MQTTConfig
		args    error
		wantErr bool
	}{
		{
			name:    "logger",
			fields:   NewMQTTConfig(),
			args: 	os.Setenv("LOGGER.LEVEL","warning"),
			wantErr: false,
		},
		{
			name:    "mqtts",
			fields:  NewMQTTConfig(),
			args:    os.Setenv("MQTTS",`[
  {
    "name": "b1",
    "broker": "tcp://127.0.0.1:1883",
    "sslca": "xxx.ca",
    "sslcert": "xxx.cert",
    "sslkey": "xxx.key",
    "insecureSkipVerify": false,
    "clientid": "client_id_xxxx",
    "username": "user_name_xxx",
    "password": "passwrd_xxx"
  },
  {
    "name": "b2",
    "broker": "tcp://127.0.0.2:1883",
    "sslca": "xxx.ca2",
    "sslcert": "xxx.cert2",
    "sslkey": "xxx.key2",
    "insecureSkipVerify": false,
    "clientid": "client_id_xxxx2",
    "username": "user_name_xxx2",
    "password": "passwrd_xxx2"
  }
]`),
			wantErr: false,
		},
		{
			name:    "rules",
			fields:   NewMQTTConfig(),
			args: 	os.Setenv("RULES",`[
  {
    "sub": {
      "name": "b1",
      "clientid": "xxxx_client_id",
      "topic": "s1",
      "qos": 1,
      "retained": true,
      "cacheid": "cahce_111"
    },
    "pub": {
      "name": "b2",
      "clientid": "xxxx_client_id",
      "topic": "s2",
      "qos": 2,
      "retained": false,
      "cacheid": "cahce_222"
    }
  },
  {
    "sub": {
      "name": "b3",
      "clientid": "xxxx_client_id",
      "topic": "s3",
      "qos": 1,
      "retained": true,
      "cacheid": "cahce_333"
    },
    "pub": {
      "name": "b4",
      "clientid": "xxxx_client_id",
      "topic": "s4",
      "qos": 2,
      "retained": false,
      "cacheid": "cahce_444"
    }
  }
]`),
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := tt.fields
			v := viper.New()
			v.AutomaticEnv()
			if err := c.BindEnv(v); (err != nil) != tt.wantErr {
				t.Errorf("BindEnv() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
