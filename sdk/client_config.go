/*
Copyright 2021 The Beijing Gridsum Technology Co., Ltd Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sdk

import (
	"errors"
	"fmt"
	"gitlab.com/taoshumin/goboot"
	"time"
)

const (
	DefaultQuiesceTimeout = 250 * time.Millisecond
	DefaultMQTTKeepAlive  = 0
)

type Config struct {
	Name string `mapstructure:"name"`
	// Broker of the MQTT Broker.
	// Valid URLs include tcp://host:port, ws://host:port or ssl://host:port.
	// If using ssl://host:port, one must also specify the SSL configuration options.
	Broker string `mapstructure:"broker"`

	// Path to CA file
	SSLCA string `mapstructure:"sslca"`
	// Path to host cert file
	SSLCert string `mapstructure:"sslcert"`
	// Path to cert key file
	SSLKey string `mapstructure:"sslkey"`
	// Use SSL but skip chain & host verification
	InsecureSkipVerify bool `mapstructure:"insecureSkipVerify"`

	// ClientID is the unique ID advertised to the MQTT broker from this client.
	// Defaults to Name if empty.
	ClientID string `mapstructure:"clientid"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`

	// newClientF is a function that returns a client for a given config.
	// It is used exclusively for testing.
	newClientF func(c Config) (goboot.Client, error) `override:"-"`
}

// SetNewClientF sets the newClientF on a Config.
// It is used exclusively for testing.
func (c *Config) SetNewClientF(fn func(c Config) (goboot.Client, error)) {
	c.newClientF = fn
}

func (c Config) Validate() error {
	if c.Name == "" {
		return errors.New("must specify a name for the mqtt broker")
	}
	if c.Broker == "" {
		return errors.New("must specify a url for mqtt service")
	}
	return nil
}

// NewClient creates a new client based off this configuration.
func (c Config) NewClient() (goboot.Client, error) {
	newC := newClient
	if c.newClientF != nil {
		newC = c.newClientF
	}
	return newC(c)
}

// SetClientID create a client id.
func (c Config) SetClientId(s string) Config {
	c.ClientID = s
	return c
}

func (c Config) Equal(o Config) bool {
	if c.Name != o.Name {
		return false
	}
	if c.Broker != o.Broker {
		return false
	}

	if c.SSLCA != o.SSLCA {
		return false
	}
	if c.SSLCert != o.SSLCert {
		return false
	}
	if c.SSLKey != o.SSLKey {
		return false
	}
	if c.InsecureSkipVerify != o.InsecureSkipVerify {
		return false
	}

	if c.ClientID != o.ClientID {
		return false
	}
	if c.Username != o.Username {
		return false
	}
	if c.Password != o.Password {
		return false
	}
	return true
}

type Configs []Config

// Validate calls config.Validate for each element in Configs
func (cs Configs) Validate() error {
	for _, c := range cs {
		err := c.Validate()
		if err != nil {
			return err
		}
	}
	return nil
}

// index generates a map of configs by name
func (cs Configs) index() map[string]Config {
	m := make(map[string]Config, len(cs))

	for _, c := range cs {
		m[HashID(c.Name, c.ClientID)] = c
	}
	return m
}

// HashFor return map key.
func HashID(n, c string) string {
	return fmt.Sprintf("%s%s", n, c)
}
